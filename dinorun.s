	ORG	$4000

	DB	'AB'
	DW	_start
	DB	0,0,0,0,0,0

	include 'bios.s'
	include 'debug.s'
	
_start:	
	call	PSG_INIT
	ld	a,1
	call	VDP_CHANGE_MODE
	
	// in which we attempt to make sprites 16x16 in size
	ld	ix,VDP_SHADOW_REG
	ld	a,(ix+1)
	or	$02
	ld	b,a
	ld	c,1
	call	VDP_WRITE_REG

	// hello world
	ld	hl,msg
	call	PUT_STRING

	// install the vsync hook
	ld	hl,vsync
	ld	a,$C3
	ld	(VSYNC_HOOK),a
	ld	(VSYNC_HOOK+1),hl

// slot_message
	ld	hl,s_slot
	call	PUT_STRING
	call	WHERE_IS_CART
	ld	c,a
	add	$30
	call	PUT_CHAR
	ld	a,c
	call	IS_SLOT_EXPANDED
	jp	z,finish_slot_check
	ld	a,'-'
	call	PUT_CHAR
	ld	a,c
	call	GET_EXPANDED_SLOT
	add	$30
	call	PUT_CHAR
finish_slot_check:
	ld	a,13
	call	PUT_CHAR
	ld	a,10
	call	PUT_CHAR
// enable_upper
	ld	hl,senamg
	call	PUT_STRING
	call	ENABLE_UPPER_PAGE
	ld	hl,mom
	call	PUT_STRING
	ld	hl,1234
	call	itoa

	xor	a
	ld	(stat_jump),a

	call	load_dino
	ld	hl,(VDP_SPRITEATTR)
	// writing some arbitrary coordinates for the dino to start in
	ld	a,$20
	call	VDP_WRITE_VRAM
	inc	hl
	call	VDP_WRITE_VRAM
	inc	hl
	ld	a,dino_stand
	call	VDP_WRITE_VRAM
	inc	hl
	ld	a,$1
	call	VDP_WRITE_VRAM
	
	ld	hl,dinostep
	ld	(hl),0

opl_reg:	equ $7C
opl_data:	equ $7D
setopldefault:
	// first I need to write to register 0x0E on the OPLL to turn off rhythm mode
	ld	c,opl_reg
	ld	a,$30
	out	(c),a
	inc	c
	out	(c),0
	nop
	nop
	nop
	nop
	nop
	nop
	// rythm should be disabled
	ld	c,opl_reg
	ld	b,8
.loop:	ld	a,b
	add	$30
	out	(c),a
	inc	c
	ld	a,$FF
	out	(c),a	//instrument F at F volume (electric guitar at max)
	dec	c
	nop	// gotta wait for 84 clocks after doing that but I figure 60 clocks of nops is enough
	nop
	nop
	nop
	nop
	nop
	djnz	.loop


loophell:
	ld	hl,SYSTEM_FRAME_CT
	ld	a,(hl)
.waitvsync:	// TODO: maybe move this all into the currently empty vsync hook
	halt
	cp	(hl)
	jp	z,.waitvsync

	call	dinostepcheck
	call	jumpcheck

	ld	hl,$0118
	call	CURSOR_POS
	ld	hl,fcount
	call	PUT_STRING
;	ld	ix,SYSTEM_FRAME_CT
;	ld	l,(ix)
;	ld	h,(ix+1)
	ld	hl,(SYSTEM_FRAME_CT)
;	ld	de,0	
	ex	de,hl
	add	hl,de

	call	itoa

	// apparently MULTIPLE TIMES I forget that this isn't RISC and you can just "ld hl,(val_at_this_mem_loc)" with an immediate
;	ld	ix,VDP_SPRITEATTR
;	ld	l,(ix)
;	ld	h,(ix+1)
	// I leave the above as a reminder to not be dumb.
	ld	hl,(VDP_SPRITEATTR)
	inc	hl
	ld	a,e
	call	VDP_WRITE_VRAM
	// at this point I think E is the x coordinate of the dino (as is A)
honk:
	cp	0
	jr	z,.honk		// I want to honk at the beginning of a line
	cp	127
	jp	p,.endhonk	// I want to silence all honks if I'm halfway across the screen
.honk:	
	ld	c,opl_reg

.endhonk:
	jp	loophell

end:
	di
	halt

jumpcheck:
	push	af
	push	hl
	ld	hl,$0117
	call	CLEAR_LINE
	xor	a
	call	JOY_BUTTON
	cp	0
	jp	z,.notrigger
	ld	hl,$0117
	call	CURSOR_POS
	ld	hl,jmptxt
	call	PUT_STRING
.notrigger:
	pop	hl
	pop	af
	ret

// TODO: Make some kind of timer event system so game objects can do a thing every so many frames
dinostepcheck:
	push	af
	push	hl
	ld	a,-10
	ld	hl,dinostep
	inc	(hl)
	add	a,(hl)
	jp	m,.finish	// if (hl) - 10 is negative we're done here (hasn't been 10 frames yet)
	ld	(hl),0		// otherwise reset the counter and begin swapping the sprite
	ld	hl,(VDP_SPRITEATTR)
	inc	hl
	inc	hl
	call	VDP_READ_VRAM
	cp	dino_left
	jp	z,.set_right
	ld	a,dino_left
	jp	.change_sprite
.set_right
	ld	a,dino_right
.change_sprite
	call	VDP_WRITE_VRAM
.finish
	pop	hl
	pop	af
	ret

// dino sprite IDs
dino_stand:	EQU $0
dino_right:	EQU $4
dino_left:	EQU $8
dino_blink:	EQU $C
dino_dead:	EQU $10

// TODO change this to use the bios block vram load routine
load_dino:
	push	af
	push	hl
	push	bc
	push	ix
	ld	hl,(VDP_SPRITETABLE)
	ld	bc,32*5
	add	hl,bc
	ld	ix,dino
	add	ix,bc
	ld	b,32*5
.loop:
	ld	a,(ix)
	call	VDP_WRITE_VRAM
	dec	hl
	dec	ix
	djnz	.loop
	pop	ix
	pop	bc
	pop	hl
	pop	af
	ret



msg:	DB	'Hello, World!',13,10,0
s_slot:	DB	'cartridge is in slot ',0
senamg:	DB	'enabling upper page...',13,10,0
fcount:	DB	'framecount: ',0
jmptxt:	DB	'JUMP!',0
dino:	include 'dinosprite.s'

vsync:
	ret

	ORG	$8000,$0
mom:	DB	"Hi mom!",13,10,0

/*--------------------------------*/
ramstart:	EQU	$E000
dinostep:	EQU	ramstart
dinoaddr:	EQU	dinostep + 2
stat_jump:	EQU	dinoaddr + 2
//eventstack:	EQU	ramstart + 1


/* NOTE NOTE NOTE
Currently thinking an event will be a 16-bit number that ticks down each frame
and when it reaches zero it just calls the address in the following 16-bit number.
I'm not sure if it makes sense to have this be some kind of list or just a block of
memory. 
could be like
uint16 count
uint16 call
uint16 next
*/
