;PSG.ENV.ONCE	EQU $00 ; _|\__
;PSG.ENV.REV	EQU $04 ; _/|__
PSG.ENV.SAW	EQU $08 	; _|\|\
PSG.ENV.ONCE	EQU $08+1	; _|\__
PSG.ENV.TRI	EQU $08+2	; _|\/\
PSG.ENV.ONCEHI	EQU $08+3	; _|\|^
PSG.ENV.REVSAW	EQU $08+4	; _/|/|
PSG.ENV.REVHI	EQU $08+5	; _/^^^
PSG.ENV.REVTRI	EQU $08+6	; _/\/\
PSG.ENV.REV	EQU $08+7	; _/|__

macro env.period ms 
	256*ms/1.787725 
endm