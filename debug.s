div10:	push	bc
	xor	a	; zero A
	scf
	ld	b,16	; gonna boop the beep 16 times.

.loop:
	adc	hl,hl	; faster rotate <<
	rla		; should pull the carry off the end of hl
	cp	10	; can we subtract 10 from this?
	jp	m,.nodice
	sub	10	; cool; actually do it then.
.nodice:
	ccf		; carry flag is the opposite of what I think it is for some reason.
	djnz	.loop

	adc	hl,hl	; zero flag set here if we're totally done
	pop	bc
	ret

;--------

itoa:	
	push	bc	
	ld	bc,0

.loop:	inc	b		; gonna need to know how many times to pop later
	call	div10		; since this loop pushes the results to the stack
	push	af
	jp	nz,.loop	; div10 will have set the zero flag if HL is empty

.unwind:	
	pop	af
	add	'0'
	call	PUT_CHAR
	djnz	.unwind

	pop	bc
	ret

