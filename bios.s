VDP_FORCE_TEXT	EQU $00D2	; does what it says on the tin
VDP_CHANGE_MODE	EQU $005F	; Desired mode in a
VDP_WRITE_REG	EQU $0047	; write the value of b to the location c
VDP_INIT_SPRITE	EQU $0069	; clears all the sprites, nice.
VDP_WRITE_VRAM	EQU $004D	; write A to HL
VDP_BLOCK_WRITE	EQU $005C	; write BC bytes of memory from HL to DE
VDP_READ_VRAM	EQU $004A	; read HL to A
VDP_BLOCK_READ	EQU $0059	; read BC bytes of memory from HL to DE
PUT_CHAR	EQU $00A2	; Desired char in a
GET_CHAR	EQU $009F	; returns char in a
CHECK_CHAR	EQU $009C	; sets z-flag if chars exist
CLEAR_LINE	EQU $0168	; clear to the end of the line from (H, L)
CURSOR_POS	EQU $00C6	; positions the cursor at (H, L)
JOY_DIRECTION	EQU $00D5	; get d-pad for controller in a, outputs to a ; KEYBDIR, P1, P2
JOY_BUTTON	EQU $00D8	; get controller button in a, outputs to a ; KEYBSPC, P1A, P2A, P1B, P2B
SLOT_ENABLE	EQU $0024	; enable a slot
READ_SLOT_MAP	EQU $0138	; returns which slots are currently selected in a. (upper memory is msbits)
SLOT_EXP_TABLE	EQU $FCC1	; start of the slot expansion status table
SYSTEM_FRAME_CT	EQU $FC9E	; jiffy increments every vsync

VDP_SHADOW_REG	EQU $F3DF	; start of VDP shadow register table (0 through 7)
VDP_SHADOW_REG8	EQU $FFE7	; continuation of VDP shadow register table (8 (MSX2))
VDP_SPRITETABLE EQU $F926	; VDP pointer for Sprite Table
VDP_SPRITEATTR	EQU $F928	; VDP pointer for Sprite Attributes
VDP_VRAM_PORT	EQU $98
VDP_CMD_PORT	EQU $99

VSYNC_HOOK	EQU $FD9F	; whatever is here will be called supposedly when a vsync happens.

PSG_INIT	EQU $0090	; init the PSG
PSG_WRITE_REG	EQU $0093	; write the value of E to the location A
PSG_READ_REG	EQU $0096	; read A from the location A
		include 'psg.s'

WHERE_IS_CART:	call 	READ_SLOT_MAP
		and	%00001100	; cartridge is always in page 1 (0x4000 - 0x7FFF)
		srl	a
		srl	a
		ret

IS_SLOT_EXPANDED: ; Checks if the slot in A is expanded. Returns 1 or 0 in A.
		push	hl
		push	bc
		ld	hl,SLOT_EXP_TABLE
		ld	c,a
		ld	b,0
		add	hl,bc		; now pointing at the subslot info for this slot.
		ld	a,(hl)
		pop	bc		; done with these registers;
		pop	hl		; we can put them back before the return logic that follows.
		and	$80		; if there's anything left after this, then the
		ret	z		; slot is expanded.
		ld	a,1		; returning 1 because I think it's nice to be all
		ret			; boolean like.

GET_EXPANDED_SLOT: ; Finds out what subslot Page 1 points to in the expanded slot named in A. Returns the subslot in A.
		push	hl
		push	bc
		ld	hl,SLOT_EXP_TABLE
		add	4		; we're gonna offset from the "expanded slot table" up to the subslot table later. Might as well do it now.
		ld	c,a
		ld	b,0
		add	hl,bc		; now pointing at the slot in question in the subslot table
		ld	a,(hl)		; it's like READ_SLOT_MAP but for subslots!
		and	%00001100	; same page 1 shit, different week
		srl	a
		srl	a
		pop	bc
		pop	hl
		ret			; the expanded subslot's Page 1 is in A.

ENABLE_UPPER_PAGE:
		call 	WHERE_IS_CART
		ld	c,a
		call	IS_SLOT_EXPANDED
		jp	z,.normal_slot
		; if so,
		ld	a,c		; gonna need the slot number back 'cause we trashed it
		call	GET_EXPANDED_SLOT
		sla	a
		sla	a
		or	c
		or	$80
		jp	.any_slot
.normal_slot:
		ld	a,c
.any_slot:
		ld	h,$80
		call	SLOT_ENABLE
		ret

PUT_STRING:	ld	a,(hl)
		cp	0
		ret	z
		inc	hl
		call	PUT_CHAR
		jr	PUT_STRING
